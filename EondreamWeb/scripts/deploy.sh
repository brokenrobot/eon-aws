#!/bin/bash

# path and s3 info about website files
WEBROOT_PATH="/mnt/c/Users/mike/source/repos/EonDreamWeb/webroot/"
WEBROOT_S3="eondream"
# cloudformation template path
CFT_PATH="/mnt/c/Users/mike/source/repos/EonDreamWeb/cloudformation/eondreamweb.yaml"


aws s3 cp "$CFT_PATH" "s3://eondata/cloudformation/EonDreamWeb/eondreamweb.yaml"
aws s3 sync --delete "$WEBROOT_PATH" "s3://$WEBROOT_S3/webroot/"