Description: website for eondream.com
Parameters:
  EondreamWebVPC:
    Description: VPC to deploy the website to.
    Type: AWS::EC2::VPC::Id
  EondreamWebSubnets:
    Description: VPC Subnets to deploy the website to.
    Type: List<AWS::EC2::Subnet::Id>
  EondreamWebSecurityGroupId:
    Description: The security group id the bastion host belongs to. Only this host will be allowed to SSH into the server.
    Type: String
  EondreamWebKeyPair:
    Description: The EC2 KeyPair used when creating instances in this deployment.
    Type: AWS::EC2::KeyPair::KeyName
  EonHostCertMgmtLog:
    Description: Name of the log group used by the custom hostname and certificate management service
    Type: String
  EondreamWebS3BucketName:
    Description: S3 bucket path that contains the files for the website.
    Type: String
    Default: eondream/webroot/
  EondreamWebServerInstanceType:
    Description: The EC2 instance type to use when creating the web server host.
    Type: String
    Default: t3.micro
  # get the latest amazon linux 2 ami id
  LatestAmiId:
    Description: This is the latest Amazon Linux 2 AMI which will be used for the EC2 instances.
    Type: 'AWS::SSM::Parameter::Value<AWS::EC2::Image::Id>'
    Default: '/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2'
Resources:
  EondreamWebServerEC2Security:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupName: EonDreamWebServerSecurityGroup
      GroupDescription: Controls access to the eondream website web ec2 servers.
      VpcId: !Ref EondreamWebVPC
  EondreamWebServerSecurityAllowHTTP:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      Description: Allow users to access the web server via HTTP.
      GroupId: !GetAtt EondreamWebServerEC2Security.GroupId
      IpProtocol: tcp
      FromPort: 80
      ToPort: 80
      CidrIp: "0.0.0.0/0"
  EondreamWebServerSecurityAllowHTTPS:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      Description: Allow users to access the web server via HTTPS.
      GroupId: !GetAtt EondreamWebServerEC2Security.GroupId
      IpProtocol: tcp
      FromPort: 443
      ToPort: 443
      CidrIp: "0.0.0.0/0"
  EondreamWebServerSecurityAllowSSH:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      Description: Allow SSH access from the bastion host.
      GroupId: !GetAtt EondreamWebServerEC2Security.GroupId
      IpProtocol: tcp
      FromPort: 22
      ToPort: 22
      SourceSecurityGroupId: !Ref EondreamWebSecurityGroupId
  EondreamWebServerEC2LaunchTemplate:
    Type: AWS::EC2::LaunchTemplate
    Properties:
      LaunchTemplateName: EondreamWebTemplate
      LaunchTemplateData:
        ImageId: !Ref LatestAmiId
        InstanceInitiatedShutdownBehavior: terminate
        InstanceType: !Ref EondreamWebServerInstanceType
        IamInstanceProfile:
          Arn: !GetAtt EondreamWebIAMProfile.Arn
        CreditSpecification:
          CpuCredits: standard
        KeyName: !Ref EondreamWebKeyPair
        SecurityGroupIds:
          - !Ref EondreamWebServerEC2Security
        UserData:
          Fn::Base64: !Sub |
            #!/bin/bash
            yum update -y
            yum install -y aws-cfn-bootstrap
            /opt/aws/bin/cfn-init --stack ${AWS::StackName} --resource EondreamWebServerEC2LaunchTemplate --region ${AWS::Region} --configsets Setup
    Metadata:
      AWS::CloudFormation::Init:
        configSets:
          Setup:
            - init
            - inst
            # - strt
        init:
          packages:
            yum:
              awslogs: []
              jq: []
              htop: []
          files:
            /etc/awslogs/awscli.conf:
              mode: "000400"
              content: !Sub |
                [plugins]
                cwlogs = cwlogs
                [default]
                region = ${AWS::Region}
            /etc/awslogs/awslogs.conf:
              mode: "000400"
              content: !Sub |
                [general]
                state_file = /var/lib/awslogs/agent-state
                use_gzip_http_content_encoding = true
                queue_size = 10

                [cfn-init]
                file = /var/log/cfn-init.log
                log_group_name = ${EondreamWebLogGroup}
                log_stream_name = cfn-init {instance_id}
                datetime_format = %Y-%m-%d %H:%M:%S,%f
                time_zone = UTC
                multi_line_start_pattern = {datetime_format}

                [sshd]
                file = /var/log/secure
                log_group_name = ${EondreamWebLogGroup}
                log_stream_name = sshd {instance_id}
                datetime_format = %b %d %H:%M:%S
            /root/r53update.sh:
              mode: "000500"
              content: !Sub |
                #!/bin/bash
                PUB_IP=$( curl http://169.254.169.254/latest/meta-data/public-ipv4 )
                JQ_SET_IP=".ChangeBatch.Changes[0].ResourceRecordSet.ResourceRecords[0].Value=\"$PUB_IP\""
                R53_JSON=$( cat /root/r53set.json | jq -cr $JQ_SET_IP )
                aws route53 change-resource-record-sets --cli-input-json "$R53_JSON"
                R53_HOSTNAME=$( cat /root/r53set.json | jq -r .ChangeBatch.Changes[0].ResourceRecordSet.Name )
                hostnamectl set-hostname "$R53_HOSTNAME"
                hostname "$R53_HOSTNAME"
            /root/r53set.json:
              mode: "000400"
              content: !Sub |
                {
                  "HostedZoneId": "${EondreamWebDnsHostedZoneId}",
                  "ChangeBatch": {
                    "Comment": "Set eondream website DNS name to the current public IP of the EC2 instance.",
                    "Changes": [{
                      "Action": "UPSERT",
                      "ResourceRecordSet": {
                        "Name": "eondreamweb.aws.eondata.net",
                        "Type": "A",
                        "TTL": 30,
                        "ResourceRecords": [ { "Value": "127.0.0.1" } ]
                      }
                    }]
                  }
                }
          commands:
            awslogsenabl:
              command: systemctl enable awslogsd
            awslogsstart:
              command: systemctl start awslogsd
            doR53update:
              command: '/root/r53update.sh'
            extraspkgs:
              command: "amazon-linux-extras install -y nginx1 epel"
        inst:
          packages:
            yum:
              certbot: []
              python2-certbot-nginx: []
          files:
            /usr/share/nginx/html/index.html:
              mode: "000444"
              content: !Sub |
                <html>
                  <head>
                    <title>eondream.com &brvbar; deflanding</title>
                    <style>
                      body { background-color: darkgray; }
                      div { margin-left: 19%;  margin-top: 22%; }
                      .disptxt { color: greenyellow; font-family: 'Courier New', Courier, monospace; font-size: 2em; }
                      .dispsym { color: rebeccapurple; font-family: Consolas, 'Lucida Console', Monaco, monospace; font-size: 7em; text-align: center; }
                    </style>
                  </head>
                  <body>
                    <div>
                      <p class="disptxt">Nothing interesting was found.</p>
                      <p class="dispsym">&dzigrarr;</p>
                    </div>
                  </body>
                </html>
          commands:
            enablwebsrv:
              command: systemctl enable nginx
            startwebsrv:
              command: systemctl start nginx
            
  EondreamWebEC2AutoScale:
    Type: AWS::AutoScaling::AutoScalingGroup
    Properties:
      AutoScalingGroupName: EondreamWebAutoScaling
      DesiredCapacity: 1
      MinSize: 0
      MaxSize: 1
      Cooldown: 90
      VPCZoneIdentifier: !Ref EondreamWebSubnets
      LaunchTemplate:
        LaunchTemplateId: !Ref EondreamWebServerEC2LaunchTemplate
        Version: 1
  EondreamWebLogGroup:
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: eondreamweb
      RetentionInDays: 120
  EondreamWebIAMRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: EondreamWebIAMRole
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - ec2.amazonaws.com
            Action:
              - sts:AssumeRole
      Path: "/"
  EondreamWebIAMProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      InstanceProfileName: EondreamWebIAMProfile
      Roles:
        - !Ref EondreamWebIAMRole
  EondreamWebIAMPolicyWriteLogs:
    Type: AWS::IAM::Policy
    Properties:
      PolicyName: EondreamWebIAMPolicyWriteLogs
      Roles:
        - !Ref EondreamWebIAMRole
      PolicyDocument:
        Version: 2012-10-17
        Statement:
          - Sid: EondreamWebIAMPolicyWriteLogs
            Effect: Allow
            Action:
              - logs:CreateLogGroup
              - logs:CreateLogStream
              - logs:DescribeLogGroups
              - logs:PutLogEvents
            Resource:
              - !Sub 'arn:aws:logs:*:*:log-group:eondreamweb:*'
  EondreamWebIAMPolicyRt53:
    Type: AWS::IAM::Policy
    Properties:
      PolicyName: EondreamWebIAMPolicyRt53
      Roles:
        - !Ref EondreamWebIAMRole
      PolicyDocument:
        Version: 2012-10-17
        Statement:
          - Sid: EondreamWebIAMPolicyRt53
            Effect: Allow
            Action:
              - route53:ChangeResourceRecordSets
            Resource:
              - !Sub 'arn:aws:route53:::hostedzone/${EondreamWebDnsHostedZoneId}'