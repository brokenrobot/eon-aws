#!/bin/bash

# get script path. all scripts should be in the same directory
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
# include common aws stuff
. $SCRIPTPATH/aws-common.sh

awsasg_display_help() {
    echo "This script is useful for managing a single host that is controlled by an autoscaling group. It isn't really suited for handling any kind of fleet of servers at all."
    echo -e "Usage:\t$0 --start <AutoScalingGroupName> : Starts the host by setting the desired capacity to one."
    echo -e "\t$0 --stop <AutoScalingGroupName> : Stops the host by setting the desired capacity to zero."
    echo -e "\t$0 --kill <AutoScalingGroupName> : Terminates the host that is in the autoscaling group. It will restart if the desired capacity of the group is more than zero."
    echo -e "\t$0 [--info] <AutoScalingGroupName> : Shows information about the autoscaling group."
    echo -e "\t$0 --help : Shows this information."
}

awsasg_set_maxcap() {
    groupName=$1
    groupCapacity=$2
    echo "Updating autoscaling group '$groupName' to a desired capacity of $groupCapacity."
    aws autoscaling set-desired-capacity --auto-scaling-group-name "$groupName" --desired-capacity "$groupCapacity"
}

# check for environment misconfiguration or request for usage info
if check_invalid_environment
then
    echo "cannot run script..."
elif [ $# = 0 ] || [ $1 = "--help" ]
then
    awsasg_display_help
else
    # set the name of the autoscaling group used by the host
    # if only one parameter is supplied then this is the same as using --info (or anything that isn't --start or --stop but whatevers)
    if [ $# = 1 ]
    then
        ASG_NAME="$1"
    else
        ASG_NAME="$2"
    fi

    # get the instance id of the ec2 host
    EC2INST_ID=$(aws autoscaling describe-auto-scaling-groups --auto-scaling-group-name "$ASG_NAME" | jq -r .AutoScalingGroups[0].Instances[0].InstanceId)
    # get the current desired capacity for the autoscaling group
    CURR_CAP=$(aws autoscaling describe-auto-scaling-groups --auto-scaling-group-names "$ASG_NAME" | jq .AutoScalingGroups[0].DesiredCapacity)

    # display the autoscaling group info
    if [ -z "$EC2INST_ID" ] || [ "$EC2INST_ID" = "null" ]
    then
        echo "The host's EC2 instance ID was not found and the capacity for the $ASG_NAME group is $CURR_CAP."    
    else
        # host is running...
        echo "The host's EC2 instance ID is $EC2INST_ID and the capacity for the $ASG_NAME group is $CURR_CAP."

        # terminate the host ec2 instance if that is what we should be doing
        if [ "$1" = "--kill" ]
        then
            echo "Killing the running host instance."
            aws ec2 terminate-instances --instance-ids "$EC2INST_ID"
        fi
    fi

    # manage the autoscale group itself
    if [ "$1" = "--start" ]
    then
        # the desired capacity is already set correctly
        if [ "$CURR_CAP" = "1" ]
        then
            # but the autoscale group desired capacity is 1
            echo "The host's autoscale group is already set to start the instance."
        else
            # update the desired capactity to start the host
            awsasg_set_maxcap $ASG_NAME 1
        fi
    elif [ "$1" = "--stop" ]
    then
        if [ "$CURR_CAP" = "0" ]
        then
            echo "The host's autoscale group is already set to not start any instances."
        else
            # update the desired capactity to start the host
            awsasg_set_maxcap "$ASG_NAME" 0
        fi
    fi
fi