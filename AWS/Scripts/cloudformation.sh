#!/bin/bash

# get script path. all scripts should be in the same directory
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

. $SCRIPTPATH/aws-common.sh

# $stackInfoJson will be a global variable. bite me
stackInfoJson=""
awscf_get_stack_info() {
    stackName=$1
    jsonFilter="select(.StackName==\"$stackName\")"
    stackInfoJson=$(aws cloudformation describe-stacks | jq .Stacks[] | jq $jsonFilter)
}

awscf_get_stack_count() {
    if [ -z "$stackInfoJson" ]
    then
        echo 0
    else
        stackCount=$(echo $stackInfoJson | jq length)
        echo $stackCount
    fi
}

# processes the stack status out of the existing $stackInfoJson variable data.
awscf_get_stack_status() {
    stackStatus=$(echo $stackInfoJson | jq --raw-output '.StackStatus')
    echo $stackStatus
}

awscf_get_param_value() {
    jsonFilter="select(.ParameterKey==\"$1\").ParameterValue"
    echo $stackInfoJson | jq '.Parameters[]' | jq --raw-output $jsonFilter
}

# waits for a cloudformation process to finish and returns either success or failure.
awscf_wait_for_finish() {
    waitStart=$SECONDS
    while [ true ]
    do
        awscf_get_stack_info $1
        currentStatus=$(awscf_get_stack_status)

        if [ "${currentStatus:(-7)}" = "_FAILED" ]
        then
            # process failed.
            return 1
        elif [ "${currentStatus:(-9)}" = "_COMPLETE" ]
        then
            # process completed. break the loop and determine if it was successful...
            break
        elif [ "${currentStatus:(-12)}" = "_IN_PROGRESS" ]
        then
            let "dur = SECONDS - waitStart"
            echo -ne "waiting for $dur seconds... current status: $currentStatus\r"
        else
            echo "unknown status while waiting for cloudformation process to finish: $currentStatus"
        fi

        sleep "${WAITFOR_DELAY}s"
    done

    if [ "${currentStatus:0:9}"  = "ROLLBACK_" ] || [ "${currentStatus:0:16}" = "UPDATE_ROLLBACK_" ]
    then
        # rollback statuses indicate something went wrong. return failure.
        return 1
    else
        # if status isn't *_FAILED or *ROLLBACK_* then return success.
        return 0
    fi
}

# display information about the tcdb cloudformation stack.
# run the aws_get_stack_info function before this one.
awscf_display_status() {
    if [ "$(awscf_get_stack_count)" = 0 ]
    then
        echo "Stack $1 not found."
    else
        stackCreated=$(echo $stackInfoJson | jq --raw-output '.CreationTime')
        currentStatus=$(awscf_get_stack_status)
        echo "The $1 stack was created at $stackCreated"
        echo "Current status: $currentStatus"
        echo "Stack parameters:"
        echo $stackInfoJson | jq --raw-output '.Parameters[] | "\t" + .ParameterKey + " = " + .ParameterValue'
    fi
}

awscf_deploy() {
    stack_name=$1
    template_directory=$2
    primary_template_filename=$3

    awscf_get_stack_info $stack_name

    if [ "$(awscf_get_stack_count)" = 0 ]
    then
        cfAct="create-stack"
    else
        cfAct="update-stack"
    fi

    awscf_upload_templates $stack_name $template_directory
    # apply the template to the cloudformation stack.
    aws cloudformation $cfAct --stack-name "$stack_name" --template-url "https://$S3BUCKET_SETUP.s3.amazonaws.com/cloudformation/$stack_name/$primary_template_filename" --capabilities CAPABILITY_NAMED_IAM
}

awscf_upload_templates() {
    # update the template stored in s3.
    aws s3 sync $2 s3://$S3BUCKET_SETUP/cloudformation/$1/
}

awscf_display_usage() {
    echo "Usage: $0 --deploy <stackname> <template_directory> <primary_template_filename> | --deploy-templates-only <stackname> <template_directory> | --delete <stackname> | --status <stackname> | --events <stackname> | --resources <stackname>"
}

if check_invalid_environment
then
    echo "cannot run script..."
elif [ $# = 0 ] || [ $1 = "--help" ]
then
    awscf_display_usage
elif [ $1 = "--deploy" ]
then
    awscf_deploy $2 $3 $4
elif [ $1 = "--deploy-templates-only" ]
then
    awscf_upload_templates $2 $3
elif [ $1 = "--delete" ]
then
    aws cloudformation delete-stack --stack-name $2
elif [ $1 = "--status" ]
then
    awscf_get_stack_info $2
    awscf_display_status $2
elif [ $1 = "--events" ]
then
    aws cloudformation describe-stack-events --stack-name $2 | jq '.[]'
elif [ $1 = "--resources" ]
then
    aws cloudformation describe-stack-resources --stack-name $2 | jq '.[]'
elif [ $1 = "--testingish" ]
then
    awscf_get_stack_info $2
    echo $stackInfoJson | jq --raw-output '.Parameters'
else
    echo "Invalid parameter."
    awscf_display_usage
fi
