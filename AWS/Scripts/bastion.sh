#!/bin/bash
ASG_NAME="EonBastionAutoScaling"

if [ $# = 0 ]
then
    SCRACT="--info"
else
    SCRACT="$1"
fi

. ./asghost.sh "$SCRACT" "$ASG_NAME"