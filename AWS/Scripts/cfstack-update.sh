#!/bin/bash
echo "Updating Eondream website stack..."
~/projects/eonaws/EonDreamWeb/scripts/deploy.sh
echo "Updating the core EonData cloudformation stack..."
. ./cloudformation.sh --deploy EonData ~/projects/eonaws/workspace/CloudFormation/Core/ eon.yaml