#!/bin/bash

#### some common shared data values ####

# name of the s3 bucket to leverage when other solutions need it for config/etc
S3BUCKET_SETUP="eondata"

#### common functions ####

# checks if the environment is configured correctly for this script to run.
check_invalid_environment() {
    jqExists=1
    awsExists=1

    if ! [ -x "$(command -v aws)" ]
    then
        echo "Error: aws cli is not installed." >&2
        awsExists=0
    fi

    if ! [ -x "$(command -v jq)" ]
    then
        echo "Error: jq is not installed." >&2
        jqExists=0
    fi

    if [ $jqExists = 0 ] || [ $awsExists = 0 ]
    then
        fixTip="To fix try: apt-get install"
        if [ $awsExists = 0 ]
        then
            fixTip="$fixTip aws"
        fi
        if [ $jqExists = 0 ]
        then
            fixTip="$fixTip jq"
        fi
        echo $fixTip
        return 0
    else
        return 1
    fi
}